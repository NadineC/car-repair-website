import { BrowserRouter, Routes, Route } from "react-router-dom";
import './App.css';
import Website from "./Website";

function App() {
  return (
    <BrowserRouter>
        <div className="App">
          <Routes>
            <Route path="/" element={<Website/>} />
          </Routes>
        </div>
      </BrowserRouter>
  );
}

export default App;
