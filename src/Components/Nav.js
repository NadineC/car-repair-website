function Nav() {
    return (
        <nav className="navbar fixed-top navbar-expand-lg bg-body-tertiary" id="navbar">
            <div className="container-fluid">
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div className="navbar-nav justify-content-end nav-pills" style={{width: "100%"}}>
                        <a className="nav-link active" aria-current="page" href="#scrollspyHome">Home</a>
                        <a className="nav-link" href="#scrollspyAboutUs">About Us</a>
                        <a className="nav-link" href="#scrollspyServices">Services</a>
                        <a className="nav-link" href="#scrollspyTestimonials">Testimonials</a>
                        <a className="nav-link" href="#scrollspyContact">Contact</a>
                    </div>
                </div>
            </div>
        </nav> 
    )
}

export default Nav;
