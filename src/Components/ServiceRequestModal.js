import { useState, useRef } from "react";
import emailjs from '@emailjs/browser';


function ServiceRequestModal() {
    const form = useRef();

    const initialFormData = {
        name: "",
        email: "",
        phone: "",
        repairLocation: "",
        serviceRequested: "",
        year: "",
        make: "",
        model: "",
        vin: "",
        comments: "",
    }
    
    const [formData, setFormData] = useState(initialFormData);

    const sendEmail = (event) => {
        event.preventDefault();

        emailjs
            .sendForm(
                'service_x9p9ns1',
                'template_i2oqbne',
                form.current, 
                {publicKey: 'csM11mvKPuZU4nz78',}
                )
            .then(
            () => {
                alert("Quote Request Sent: Thank you for your request, an associate will respond as soon as possible.");
                setFormData(initialFormData)
            },
            (error) => {
                console.log('FAILED...', error.text);
                alert("Message could not be sent, please try again or give us a call")
            });
    }

    const handleFormChange = async (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    }

    return (
        <>
        {/* Button trigger modal */}
            <button type="button" className="btn btn-primary" data-bs-toggle="modal" data-bs-target="#quoteModal">Request a quote</button>
            
        	{/* Modal */}
            <div className="modal fade" id="quoteModal" tabIndex="-1" aria-labelledby="quoteModalLabel" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h1 className="modal-title fs-5" id="quoteModalLabel">Request Quote</h1>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <form ref={form} onSubmit={sendEmail} className="modal-body">
                            <div>
                                <label htmlFor="name" className="form-label">Name</label>
                                <input 
                                    placeholder="Name"
                                    onChange={handleFormChange}
                                    required
                                    type="text"
                                    name="name" 
                                    id="name"                    
                                    className="form-control" 
                                    value={formData.name}
                                />
                            </div>
                            <div>
                                <label htmlFor="email" className="form-label">Email</label>
                                <input 
                                    placeholder="Email"
                                    onChange={handleFormChange}
                                    type="email"
                                    name="email" 
                                    id="email"                    
                                    className="form-control" 
                                    value={formData.email}
                                />
                            </div>
                            <div>
                                <label htmlFor="phone" className="form-label">Phone Number</label>
                                <input 
                                    placeholder="Phone Number"
                                    onChange={handleFormChange}
                                    required
                                    type="phone"
                                    name="phone" 
                                    id="phone"                    
                                    className="form-control" 
                                    value={formData.phone}
                                />
                            </div>
                            <div>
                                <label htmlFor="repairLocation" className="form-label">Repair Location (City, State)</label>
                                <input 
                                    placeholder="Repair Location"
                                    onChange={handleFormChange}
                                    required
                                    type="text"
                                    name="repairLocation" 
                                    id="repairLocation"                    
                                    className="form-control" 
                                    value={formData.repairLocation}
                                />            
                            </div>
                            <div>
                                <label htmlFor="serviceRequested" className="form-label">Service Requested</label>
                                <input 
                                    placeholder="Service Requested"
                                    onChange={handleFormChange}
                                    required
                                    type="text"
                                    name="serviceRequested" 
                                    id="serviceRequested"                    
                                    className="form-control" 
                                    value={formData.serviceRequested}
                                /> 
                            </div>
                            <div>
                                <label htmlFor="year" className="form-label">Year</label>
                                <input 
                                    placeholder="Year"
                                    onChange={handleFormChange}
                                    required
                                    type="text"
                                    name="year" 
                                    id="year"                    
                                    className="form-control" 
                                    value={formData.year}
                                />
                            </div>
                            <div>
                                <label htmlFor="make" className="form-label">Make</label>
                                <input 
                                    placeholder="Make"
                                    onChange={handleFormChange}
                                    required
                                    type="text"
                                    name="make" 
                                    id="make"                    
                                    className="form-control" 
                                    value={formData.make}
                                />
                            </div>
                            <div>
                                <label htmlFor="model" className="form-label">Model</label>
                                <input 
                                    placeholder="Model"
                                    onChange={handleFormChange}
                                    required
                                    type="text"
                                    name="model" 
                                    id="model"                    
                                    className="form-control" 
                                    value={formData.model}
                                />
                            </div>
                            <div>
                                <label htmlFor="vin" className="form-label">Vin</label>
                                <input 
                                    placeholder="Vin"
                                    onChange={handleFormChange}
                                    type="text"
                                    name="vin" 
                                    id="vin"                    
                                    className="form-control" 
                                    value={formData.vin}
                                />
                            </div>
                            <div>
                                <label htmlFor="comments" className="form-label">Comments</label>
                                <input 
                                    placeholder="Comments"
                                    onChange={handleFormChange}
                                    required
                                    type="text"
                                    name="comments" 
                                    id="comments"                    
                                    className="form-control" 
                                    value={formData.comments}
                                />
                            </div>
                            <div className="modal-footer">
                                <button type="submit" className="btn btn-primary" value="Send">Request Quote</button>
                                <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    );
}
export default ServiceRequestModal;
