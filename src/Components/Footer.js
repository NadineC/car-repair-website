import data from "../client.json";

function Footer() {
    return (
        <footer id="scrollspyContact">
                    <div className="row align-items-center">
                        <div className="col">
                            <table>                        
                                <tbody>
                                    <tr>
                                        <th>Hours</th>
                                    </tr>
                                    <tr >
                                        <td>Monday</td>
                                        <td>8:00am - 6:00pm</td>
                                    </tr>
                                    <tr>
                                        <td>Tuesday</td>
                                        <td>8:00am - 6:00pm</td>
                                    </tr>
                                    <tr>
                                        <td>Wednesday</td>
                                        <td>8:00am - 6:00pm</td>
                                    </tr>
                                    <tr>
                                        <td>Thursday</td>
                                        <td>8:00am - 6:00pm</td>
                                    </tr>
                                    <tr>
                                        <td>Friday</td>
                                        <td>8:00am - 6:00pm</td>
                                    </tr>
                                    <tr>
                                        <td>Saturday</td>
                                        <td>8:00am - 3:00pm</td>
                                    </tr>
                                    <tr>
                                        <td>Sunday</td>
                                        <td>Closed</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div className="col">
                            <h5>Accepted Payments</h5>
                            <div>Cash, Check, Visa, MasterCard, Discover</div>
                            <div>
                                <img style={{width: "10%", height: "auto"}} src="./visa-icon.png" alt="Visa Icon"/>
                                <img style={{width: "10%", height: "auto"}} src="./master-card-icon.png" alt="Master Card Icon"/>
                                <img style={{width: "10%", height: "auto"}} src="./discover-icon.png" alt="Discover Icon"/>
                            </div>
                        </div>
                        <div className="col">
                            <div>{data.street}</div>
                            <div>{data.cityState} {data.zip}</div>
                            <div>{data.phone}</div>
                        </div>
                        
                    </div>
                </footer>
    )
}

export default Footer;
