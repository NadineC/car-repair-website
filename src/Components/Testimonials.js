import {useState, useEffect} from 'react';
import data from "../yelp.json"

function Testimonials() {
    const [reviews, setReviews] = useState([]);
    const [loading, setLoading] = useState(false);

    const options = {
        method: 'GET',
        headers: {
            accept: "application/json",
            Authorization: `${process.env.REACT_APP_YELP_API_KEY}`,
            "Access-Control-Allow-Origin": "*",
            origin: null,
        }
    };
    
    
    useEffect(() => {
        const getData = async () => {
            setLoading(true);
            const yelp_url = 'https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/arts-auto-glass-chula-vista-2/reviews?locale=en_US&limit=20&sort_by=yelp_sort';
            const response = await fetch(yelp_url, options);
            
            const data = await response.json();
            setReviews(data.reviews);
            setLoading(false);        
        };
        getData();            
    }, []);
    return(
        <div id="carouselReviewControls" className="carousel slide" data-bs-ride="carousel">
            <div className="carousel-inner container text-center">
                {loading ? (
                    <div className="spinner-border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </div>
                    ) : (
                    <> 
                            {data.reviews.map((review, index) => {
                                return (
                                    <div key={review.id} className={`carousel-item ${index === 0 ? "active" : ""}`}>
                                        <div class="card">
                                            <h5 class="card-testimonials">{ review.rating } Star</h5>
                                            <div class="card-body">
                                                <p class="card-text">{ review.text }<a href={`{review.url}`} class="btn btn-link">Read more on Yelp</a></p>
                                                <h5 class="card-title">{ review.user.name }</h5>
                                                <img src="./yelp_logo.png" style={{width: "20%", height: "auto"}} alt="Yelp Logo"/>
                                            </div>
                                        </div>
                                    </div> 
                                )
                            })}
                    </>
                )}                
            </div>
            <button className="carousel-control-prev" type="button" data-bs-target="#carouselReviewControls" data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target="#carouselReviewControls" data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>                
        </div>
    );
}
export default Testimonials;
