import data from "../client.json";

function Header() {
    return (                
        <div className="container header position-relative mt-5">
            <div className="logo-bg position-absolute top-0 start-0 z-2 bg-light opacity-50"/>
            <div className="logo position-absolute top-0 start-0 z-3">
                <img className="logo" src="./logo.png" alt={data.company}/>
                <div>{data.cityState} | {data.phone}</div>
            </div>
            <div id="carouselWorkControls" className="carousel slide position-absolute top-0 start-0 z-1" data-bs-ride="carousel">
                <div className="carousel-inner work">
                    <div className="carousel-item active work">
                        <img src="https://s3-media0.fl.yelpcdn.com/bphoto/Uq9Mon7uaRbNnN5MU3FwhQ/o.jpg" className="d-block w-100" alt="Craftsmanship"/>
                    </div>
                    <div className="carousel-item work">
                        <img src="https://s3-media0.fl.yelpcdn.com/bphoto/HrK4w49rCvT7EC-ERL-88A/o.jpg" className="d-block w-100" alt="Craftsmanship"/>
                    </div>
                    <div className="carousel-item work">
                        <img src="https://s3-media0.fl.yelpcdn.com/bphoto/ufTP_dnQIwcTod1dQNbDhQ/o.jpg" className="d-block w-100" alt="Craftsmanship"/>
                    </div>
                    <div className="carousel-item work">
                        <img src="https://s3-media0.fl.yelpcdn.com/bphoto/2LUVSRQ1paO-0bmDFkqiNA/o.jpg" className="d-block w-100" alt="Craftsmanship"/>
                    </div>
                </div>
                <button className="carousel-control-prev" type="button" data-bs-target="#carouselWorkControls" data-bs-slide="prev">
                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Previous</span>
                </button>
                <button className="carousel-control-next" type="button" data-bs-target="#carouselWorkControls" data-bs-slide="next">
                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                    <span className="visually-hidden">Next</span>
                </button>
            </div>
        </div>  
    );
    
}
export default Header;
