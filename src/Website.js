import Nav from "./Components/Nav";
import Header from "./Components/Header";
import data from "./client.json";
import Testimonials from "./Components/Testimonials";
import Footer from "./Components/Footer";
import ServiceRequestModal from "./Components/ServiceRequestModal";

function Website() {
    return(
        <>
        <div data-bs-spy="scroll" data-bs-target="#navbar" data-bs-root-margin="0px 0px -40%" data-bs-smooth-scroll="true" className="scrollspy-example bg-light p-3 rounded-2" tabindex="0">
            <Nav/>
            <div id="scrollspyHome">
                <Header/>
            </div>
            <ServiceRequestModal/>
            <div className="px-4 py-5 my-5 text-center">
                <h1 className="display-5 fw-bold">Here to help</h1>
                <div className="col-lg-6 mx-auto">
                </div>
                <div className="about" id="scrollspyAboutUs">
                    <h2>About Us</h2>
                    <p>{data.about}</p>
                    <h2>History</h2>
                    <p>{data.history}</p>
                    <h2>Meet the Owner</h2>
                    <p>{data.owner}</p>
                </div>
                <div className="services" id="scrollspyServices">
                    <h2>Services</h2>
                    <ul className="servicesList">
                        {data.services.map((service) => <li>{service.name}</li>)}
                    </ul>
                </div>
                <div id="scrollspyTestimonials">
                    <Testimonials/>
                </div>
                <div id="scrollspyContact">
                    <Footer/>
                </div>
            </div>
        </div>
        </>
    );
}

export default Website;
